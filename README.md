# Palemoon_FreeBSD_port

A resurrection of an abandoned Palemoon browser port for FreeBSD.

Due to weird python 2.7 deprecation policies, this package has been deletted from FreeBSD ports tree, even though, it is still buildable and requires literally no changes to get resurrected.
To get the last original SVN revision prior to port deletion, please clone with the following command:
```
svn checkout https://svn.FreeBSD.org/ports/head/www@565349
```
Otherwise - follow our project and build th current Palemoon version:
```
git clone https://gitlab.com/megastallman/palemoon_freebsd_port.git/
cd palemoon_freebsd_port/palemoon
make install clean
```
All the build and runtime dependencies are still available for FreeBSD 12.2 at the moment of writing.
